const Configs = {
    Authorization: {
        url: 'http://51.195.41.192:4000',
        bearerSchema: 'Bearer ',
        basicSchema: 'Basic ',
        json : 'application/json',
        jsonEncoded : 'application/x-www-form-urlencoded',  
        introSpect: 'YnV0dGVyZmx5X21hbmFnZW1lbnRfc2VydmljZTpibS1zZXJ2aWNl'
    }
};
export default Configs;